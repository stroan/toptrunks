import com.eclecticdesignstudio.motion.Actuate;
import nme.display.Sprite;
import nme.display.Bitmap;
import nme.Assets;
import nme.events.MouseEvent;
import nme.geom.Point;
import nme.Lib;


class Main extends Sprite {
    var playerHand : PlayerHand;
    var enemyHand : EnemyHand;

    var playerCardCentre : Point;
    var enemyCardCentre : Point;

    var playerCard : Card;
    var enemyCard : Card;
    
    public function new () {
        super ();

        var background = new Sprite();
        background.addChild(new Bitmap(Assets.getBitmapData("assets/background.png")));
        background.addEventListener(MouseEvent.CLICK, backgroundClick);
        addChild(background);

        playerHand = new PlayerHand();
        playerHand.cardPicked = playerCardPicked;
        addChild(playerHand);

        enemyHand = new EnemyHand();
        addChild(enemyHand);

        playerCardCentre = new Point(Lib.current.stage.stageWidth / 2 - 300,
                                     Lib.current.stage.stageHeight / 2);
        enemyCardCentre = new Point(playerCardCentre.x + 600,
                                    playerCardCentre.y);
    }

    public function backgroundClick(event:MouseEvent) {
        playerHand.deselectCard();
    }
    
    public function playerCardPicked(c:Card) {
        playerCard = c;
        playerHand.removeCard(c);
        playerHand.interactionEnabled = false;

        Actuate.tween(c, 1, {x: playerCardCentre.x - (c.width / 2), 
                             y: playerCardCentre.y - (c.height / 2)}, false).onComplete(playerCardCentred);
    }

    public function playerCardCentred() {
        var c = enemyHand.pickCard();
        enemyCard = c;
        enemyHand.removeCard(c);

        Actuate.tween(c, 1, {x: enemyCardCentre.x - (c.width / 2),
                             y: enemyCardCentre.y - (c.height / 2)}, false).onComplete(enemyCardCentred);
        Lib.trace("Centred");
    }

    public function enemyCardCentred() {
        enemyCard.setShowFace(true);
        Actuate.timer(2).onComplete(completeFight);
    }

    public function completeFight() {
        playerHand.replaceCard(playerCard);
        enemyHand.replaceCard(enemyCard);
        enemyCard.setShowFace(false);
        playerHand.interactionEnabled = true;
    }    
}