import nme.display.Sprite;
import nme.display.Bitmap;
import nme.text.TextField;
import nme.events.MouseEvent;
import nme.Assets;
import nme.Lib;

class Card extends Sprite {
    public var uid : Int;
    var spec : CardSpec;
    var face : Bitmap;
    var back : Bitmap;

    var strengthText : TextField;
    var magicText : TextField;
    var speedText : TextField;

    public var selectEvent : Card -> Void;



    public function new (spec) {
        super();

        this.spec = spec;

        uid = CardIdGenerator.genId();
        face = new Bitmap(Assets.getBitmapData(spec.img));
        addChild(face);

        back = new Bitmap(Assets.getBitmapData('assets/CardBack.png'));
        addChild(back);

        strengthText = makeStatText("STRENGTH: " + spec.strength, 230);
        magicText = makeStatText("MAGIC: " + spec.magic, 255);
        speedText = makeStatText("SPEED: " + spec.speed, 280);

        setShowFace(true);
    }

    public function makeStatText(str, y) {
        var field = new TextField();
        field.x = 90;
        field.y = y;
        field.height = 25;
        field.text = str;
        field.textColor = 0xFFFFFFFF;
        field.mouseEnabled = false;
        return field;
    }

    public function registerEvents() {
        addEventListener(MouseEvent.CLICK, onMouseClick);
    }

    public function setShowFace(showFace) {
        face.visible = showFace;
        back.visible = !showFace;

        if (showFace) {
            addChild(strengthText);
            addChild(magicText);
            addChild(speedText);
        } else {
            removeChild(strengthText);
            removeChild(magicText);
            removeChild(speedText);
        }
    }

    private function onMouseClick(event:MouseEvent) {
        if (selectEvent != null) {
            selectEvent(this);
        }
    }
}