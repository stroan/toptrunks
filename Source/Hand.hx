import com.eclecticdesignstudio.motion.Actuate;
import nme.display.Sprite;
import nme.geom.Point;
import nme.Lib;

class Hand extends Sprite {
    var cardSpacing = 100;
    var yOffset = 250;

    var centreX : Int;
    var centreY : Int;

    var cards : Array<Card>;
    var selectedCard : Card = null;

    public function new (yPos) {
        super();

        
        centreX = Std.int(Lib.current.stage.stageWidth / 2);
        centreY = yPos;

        cards = new Array<Card>();
        newCard(CardSpec.getSpecs().get('LEAF_WIZARD').makeInstance());
        newCard(CardSpec.getSpecs().get('MIGHTY_MITE').makeInstance());
        newCard(CardSpec.getSpecs().get('MOON_BLOSSOM').makeInstance());
        newCard(CardSpec.getSpecs().get('CHOSEN_WORM').makeInstance());

        arrangeCards(false);
    }

    public function newCard(card : Card) {
        addChild(card);
        cards.push(card);
    }

    public function arrangeCards(tween: Bool) {
        var cardWidth = cards[0].width;
        var totalWidth = cardWidth + ((cards.length - 1) * cardSpacing);

        var lastCard = cards[cards.length - 1];

        if (selectedCard != null && lastCard != selectedCard){
            totalWidth += cardWidth;
        }

        var currentX = centreX - (totalWidth / 2);
        for (card in cards) {
            var destX = currentX;
            var destY = centreY - (card.height / 2);
            currentX += cardSpacing;
            if (card == selectedCard) {
                currentX += cardWidth;
                destY -= yOffset;
            }
            if (tween) {
                Actuate.tween(card, 1, {x: destX, y: destY}, false);
            } else {
                card.x = destX;
                card.y = destY;
            }
        }
    }

    public function removeCard(c:Card) {
        cards.remove(c);
        if (selectedCard == c) {
            selectedCard = null;
        }
        arrangeCards(true);
    }

    public function replaceCard(c:Card) {
        var prev = -100;

        var index = 0;

        for (current in cards) {
            if (prev < c.uid && current.uid > c.uid) {
                break;
            }
            index++;
        }

        cards.insert(index, c);
        arrangeCards(true);
    }
}