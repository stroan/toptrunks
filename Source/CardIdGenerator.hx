class CardIdGenerator {
    static var currentId : Int = 0;

    public static function genId() : Int {
        return currentId++;
    }
}