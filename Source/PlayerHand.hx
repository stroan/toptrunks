import nme.Lib;

class PlayerHand extends Hand {
	public var cardPicked : Card -> Void;
	public var interactionEnabled : Bool = true;

	public function new() {
		super(Lib.current.stage.stageHeight);
	}

	public override function newCard(c : Card) {
		super.newCard(c);
        c.setShowFace(true);
		c.registerEvents();
		c.selectEvent = cardSelect;
	}

    public function cardSelect(c : Card) {
    	if (!interactionEnabled) {
    		return;
    	}
    	
    	if (c != selectedCard) {
	        selectedCard = c;
	        arrangeCards(true);
    	} else {
    		if (cardPicked != null) {
    			cardPicked(c);
    		}
    	}
    }

    public function deselectCard() {
        selectedCard = null;
        arrangeCards(true);
    }
}