class CardSpec {
	public var img : String;
	public var strength : Int;
	public var magic : Int;
	public var speed : Int;

	private static var specs : Hash<CardSpec> = null;

	public function new(img, strength, magic, speed) {
		this.img = img;
		this.strength = strength;
		this.magic = magic;
		this.speed = speed;
	}

	public function makeInstance() {
		return new Card(this);
	}

	public static function getSpecs() {
		if (specs == null) {
			specs = new Hash();
			specs.set('LEAF_WIZARD', new CardSpec("assets/LeafWizardCard.png", 7, 2, 1));
			specs.set('MIGHTY_MITE', new CardSpec("assets/MightyMiteCard.png", 7, 2, 1));
			specs.set('MOON_BLOSSOM', new CardSpec("assets/MoonBlossomCard.png", 7, 2, 1));
			specs.set('TREE_BROWS', new CardSpec("assets/TreeBrowsCard.png", 7, 2, 1));
			specs.set('CHOSEN_WORM', new CardSpec("assets/ChosenWormCard.png", 7, 2, 1));
		}
		return specs;
	}
}