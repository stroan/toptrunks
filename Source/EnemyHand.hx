class EnemyHand extends Hand {
	public function new() {
		super(0);
	}

	public override function newCard(c : Card) {
		super.newCard(c);
        c.setShowFace(false);
	}

	public function pickCard() {
		return cards[Std.random(cards.length)];
	}
}